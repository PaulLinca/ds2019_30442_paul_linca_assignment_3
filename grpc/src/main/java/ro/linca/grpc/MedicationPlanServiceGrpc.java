package ro.linca.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: MedicationPlan.proto")
public final class MedicationPlanServiceGrpc {

  private MedicationPlanServiceGrpc() {}

  public static final String SERVICE_NAME = "MedicationPlanService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<ro.linca.grpc.MedicationPlan.EmptyRequest,
      ro.linca.grpc.MedicationPlan.GetPlanResponse> getGetPlanMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getPlan",
      requestType = ro.linca.grpc.MedicationPlan.EmptyRequest.class,
      responseType = ro.linca.grpc.MedicationPlan.GetPlanResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ro.linca.grpc.MedicationPlan.EmptyRequest,
      ro.linca.grpc.MedicationPlan.GetPlanResponse> getGetPlanMethod() {
    io.grpc.MethodDescriptor<ro.linca.grpc.MedicationPlan.EmptyRequest, ro.linca.grpc.MedicationPlan.GetPlanResponse> getGetPlanMethod;
    if ((getGetPlanMethod = MedicationPlanServiceGrpc.getGetPlanMethod) == null) {
      synchronized (MedicationPlanServiceGrpc.class) {
        if ((getGetPlanMethod = MedicationPlanServiceGrpc.getGetPlanMethod) == null) {
          MedicationPlanServiceGrpc.getGetPlanMethod = getGetPlanMethod = 
              io.grpc.MethodDescriptor.<ro.linca.grpc.MedicationPlan.EmptyRequest, ro.linca.grpc.MedicationPlan.GetPlanResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MedicationPlanService", "getPlan"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.linca.grpc.MedicationPlan.EmptyRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.linca.grpc.MedicationPlan.GetPlanResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MedicationPlanServiceMethodDescriptorSupplier("getPlan"))
                  .build();
          }
        }
     }
     return getGetPlanMethod;
  }

  private static volatile io.grpc.MethodDescriptor<ro.linca.grpc.MedicationPlan.MedicationTakenRequest,
      ro.linca.grpc.MedicationPlan.MedicationTakenResponse> getNotifyServerMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "notifyServer",
      requestType = ro.linca.grpc.MedicationPlan.MedicationTakenRequest.class,
      responseType = ro.linca.grpc.MedicationPlan.MedicationTakenResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ro.linca.grpc.MedicationPlan.MedicationTakenRequest,
      ro.linca.grpc.MedicationPlan.MedicationTakenResponse> getNotifyServerMethod() {
    io.grpc.MethodDescriptor<ro.linca.grpc.MedicationPlan.MedicationTakenRequest, ro.linca.grpc.MedicationPlan.MedicationTakenResponse> getNotifyServerMethod;
    if ((getNotifyServerMethod = MedicationPlanServiceGrpc.getNotifyServerMethod) == null) {
      synchronized (MedicationPlanServiceGrpc.class) {
        if ((getNotifyServerMethod = MedicationPlanServiceGrpc.getNotifyServerMethod) == null) {
          MedicationPlanServiceGrpc.getNotifyServerMethod = getNotifyServerMethod = 
              io.grpc.MethodDescriptor.<ro.linca.grpc.MedicationPlan.MedicationTakenRequest, ro.linca.grpc.MedicationPlan.MedicationTakenResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MedicationPlanService", "notifyServer"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.linca.grpc.MedicationPlan.MedicationTakenRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.linca.grpc.MedicationPlan.MedicationTakenResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MedicationPlanServiceMethodDescriptorSupplier("notifyServer"))
                  .build();
          }
        }
     }
     return getNotifyServerMethod;
  }

  private static volatile io.grpc.MethodDescriptor<ro.linca.grpc.MedicationPlan.EmptyRequest,
      ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse> getAlertServerMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "alertServer",
      requestType = ro.linca.grpc.MedicationPlan.EmptyRequest.class,
      responseType = ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ro.linca.grpc.MedicationPlan.EmptyRequest,
      ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse> getAlertServerMethod() {
    io.grpc.MethodDescriptor<ro.linca.grpc.MedicationPlan.EmptyRequest, ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse> getAlertServerMethod;
    if ((getAlertServerMethod = MedicationPlanServiceGrpc.getAlertServerMethod) == null) {
      synchronized (MedicationPlanServiceGrpc.class) {
        if ((getAlertServerMethod = MedicationPlanServiceGrpc.getAlertServerMethod) == null) {
          MedicationPlanServiceGrpc.getAlertServerMethod = getAlertServerMethod = 
              io.grpc.MethodDescriptor.<ro.linca.grpc.MedicationPlan.EmptyRequest, ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MedicationPlanService", "alertServer"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.linca.grpc.MedicationPlan.EmptyRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MedicationPlanServiceMethodDescriptorSupplier("alertServer"))
                  .build();
          }
        }
     }
     return getAlertServerMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MedicationPlanServiceStub newStub(io.grpc.Channel channel) {
    return new MedicationPlanServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MedicationPlanServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new MedicationPlanServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MedicationPlanServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new MedicationPlanServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class MedicationPlanServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getPlan(ro.linca.grpc.MedicationPlan.EmptyRequest request,
        io.grpc.stub.StreamObserver<ro.linca.grpc.MedicationPlan.GetPlanResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetPlanMethod(), responseObserver);
    }

    /**
     */
    public void notifyServer(ro.linca.grpc.MedicationPlan.MedicationTakenRequest request,
        io.grpc.stub.StreamObserver<ro.linca.grpc.MedicationPlan.MedicationTakenResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getNotifyServerMethod(), responseObserver);
    }

    /**
     */
    public void alertServer(ro.linca.grpc.MedicationPlan.EmptyRequest request,
        io.grpc.stub.StreamObserver<ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getAlertServerMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetPlanMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ro.linca.grpc.MedicationPlan.EmptyRequest,
                ro.linca.grpc.MedicationPlan.GetPlanResponse>(
                  this, METHODID_GET_PLAN)))
          .addMethod(
            getNotifyServerMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ro.linca.grpc.MedicationPlan.MedicationTakenRequest,
                ro.linca.grpc.MedicationPlan.MedicationTakenResponse>(
                  this, METHODID_NOTIFY_SERVER)))
          .addMethod(
            getAlertServerMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ro.linca.grpc.MedicationPlan.EmptyRequest,
                ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse>(
                  this, METHODID_ALERT_SERVER)))
          .build();
    }
  }

  /**
   */
  public static final class MedicationPlanServiceStub extends io.grpc.stub.AbstractStub<MedicationPlanServiceStub> {
    private MedicationPlanServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MedicationPlanServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MedicationPlanServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MedicationPlanServiceStub(channel, callOptions);
    }

    /**
     */
    public void getPlan(ro.linca.grpc.MedicationPlan.EmptyRequest request,
        io.grpc.stub.StreamObserver<ro.linca.grpc.MedicationPlan.GetPlanResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetPlanMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void notifyServer(ro.linca.grpc.MedicationPlan.MedicationTakenRequest request,
        io.grpc.stub.StreamObserver<ro.linca.grpc.MedicationPlan.MedicationTakenResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getNotifyServerMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void alertServer(ro.linca.grpc.MedicationPlan.EmptyRequest request,
        io.grpc.stub.StreamObserver<ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAlertServerMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class MedicationPlanServiceBlockingStub extends io.grpc.stub.AbstractStub<MedicationPlanServiceBlockingStub> {
    private MedicationPlanServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MedicationPlanServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MedicationPlanServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MedicationPlanServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ro.linca.grpc.MedicationPlan.GetPlanResponse getPlan(ro.linca.grpc.MedicationPlan.EmptyRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetPlanMethod(), getCallOptions(), request);
    }

    /**
     */
    public ro.linca.grpc.MedicationPlan.MedicationTakenResponse notifyServer(ro.linca.grpc.MedicationPlan.MedicationTakenRequest request) {
      return blockingUnaryCall(
          getChannel(), getNotifyServerMethod(), getCallOptions(), request);
    }

    /**
     */
    public ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse alertServer(ro.linca.grpc.MedicationPlan.EmptyRequest request) {
      return blockingUnaryCall(
          getChannel(), getAlertServerMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class MedicationPlanServiceFutureStub extends io.grpc.stub.AbstractStub<MedicationPlanServiceFutureStub> {
    private MedicationPlanServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MedicationPlanServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MedicationPlanServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MedicationPlanServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ro.linca.grpc.MedicationPlan.GetPlanResponse> getPlan(
        ro.linca.grpc.MedicationPlan.EmptyRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetPlanMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ro.linca.grpc.MedicationPlan.MedicationTakenResponse> notifyServer(
        ro.linca.grpc.MedicationPlan.MedicationTakenRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getNotifyServerMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse> alertServer(
        ro.linca.grpc.MedicationPlan.EmptyRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getAlertServerMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_PLAN = 0;
  private static final int METHODID_NOTIFY_SERVER = 1;
  private static final int METHODID_ALERT_SERVER = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MedicationPlanServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MedicationPlanServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_PLAN:
          serviceImpl.getPlan((ro.linca.grpc.MedicationPlan.EmptyRequest) request,
              (io.grpc.stub.StreamObserver<ro.linca.grpc.MedicationPlan.GetPlanResponse>) responseObserver);
          break;
        case METHODID_NOTIFY_SERVER:
          serviceImpl.notifyServer((ro.linca.grpc.MedicationPlan.MedicationTakenRequest) request,
              (io.grpc.stub.StreamObserver<ro.linca.grpc.MedicationPlan.MedicationTakenResponse>) responseObserver);
          break;
        case METHODID_ALERT_SERVER:
          serviceImpl.alertServer((ro.linca.grpc.MedicationPlan.EmptyRequest) request,
              (io.grpc.stub.StreamObserver<ro.linca.grpc.MedicationPlan.MedicationNotTakenResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class MedicationPlanServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    MedicationPlanServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ro.linca.grpc.MedicationPlan.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("MedicationPlanService");
    }
  }

  private static final class MedicationPlanServiceFileDescriptorSupplier
      extends MedicationPlanServiceBaseDescriptorSupplier {
    MedicationPlanServiceFileDescriptorSupplier() {}
  }

  private static final class MedicationPlanServiceMethodDescriptorSupplier
      extends MedicationPlanServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    MedicationPlanServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MedicationPlanServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MedicationPlanServiceFileDescriptorSupplier())
              .addMethod(getGetPlanMethod())
              .addMethod(getNotifyServerMethod())
              .addMethod(getAlertServerMethod())
              .build();
        }
      }
    }
    return result;
  }
}
