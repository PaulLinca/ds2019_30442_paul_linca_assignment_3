package ro.linca.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import ro.linca.service.MedicationPlanService;

import java.io.IOException;

public class GRPCServer
{
    public static void main(String[] args) throws IOException, InterruptedException
    {
        Server server = ServerBuilder
                .forPort(9090)
                .addService(new MedicationPlanService())
                .build();

        server.start();
        System.out.println("Server started at port " + server.getPort() + ".");
        server.awaitTermination();
    }
}
