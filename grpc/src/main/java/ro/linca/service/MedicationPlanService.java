package ro.linca.service;

import io.grpc.stub.StreamObserver;
import ro.linca.connection.MedicationPlanDao;
import ro.linca.grpc.MedicationPlan;
import ro.linca.grpc.MedicationPlanServiceGrpc;

import java.sql.SQLException;

public class MedicationPlanService extends MedicationPlanServiceGrpc.MedicationPlanServiceImplBase
{
    @Override
    public void getPlan(MedicationPlan.EmptyRequest request, StreamObserver<MedicationPlan.GetPlanResponse> responseObserver)
    {
        System.out.println("Fetching medication plans...");

        MedicationPlan.GetPlanResponse.Builder response = MedicationPlan.GetPlanResponse.newBuilder();
        try
        {
            String plans = MedicationPlanDao.getAllPlans();
            response.setResponseCode(200).setResponseMessage("Success.").setMedicationPlanJson(plans);
        }
        catch (SQLException e)
        {
            response.setResponseCode(400).setResponseMessage("Failed");
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void notifyServer(MedicationPlan.MedicationTakenRequest request, StreamObserver<MedicationPlan.MedicationTakenResponse> responseObserver)
    {
        System.out.println("Medication taken by patient:\n\t" + request.getRequestMessage());

        MedicationPlan.MedicationTakenResponse.Builder response = MedicationPlan.MedicationTakenResponse.newBuilder();
        response
            .setResponseCode(200)
            .setResponseMessage("Server notified");
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void alertServer(MedicationPlan.EmptyRequest request, StreamObserver<MedicationPlan.MedicationNotTakenResponse> responseObserver)
    {
        System.out.println("Patient didn't take his/her medication!\n");

        MedicationPlan.MedicationNotTakenResponse.Builder response = MedicationPlan.MedicationNotTakenResponse.newBuilder();
        response
                .setResponseCode(200)
                .setResponseMessage("Server alerted");
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }
}
