package ro.linca.connection;

import com.google.gson.Gson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MedicationPlanDao
{

    private static Connection connectToDb()
    {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.createConnection();

        return connectionFactory.getConnection();
    }

    public static String getAllPlans() throws SQLException
    {
        String selectAllQuery= "SELECT * FROM medicalmonitor.prescription p INNER JOIN medicalmonitor.drug d ON p.drug_id = d.id";

        Connection connection = connectToDb();
        PreparedStatement preparedStatement = connection.prepareStatement(selectAllQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        System.out.println("\tFetched prescriptions: ");

        List<String> prescriptions = new ArrayList<>();
        int count = 0;
        while(resultSet.next())
        {
            count++;
            System.out.println("\t\tPrescription " + count + ": " + resultSet.getString("name") + " " + resultSet.getString("amount_in_grams") + " " + resultSet.getString("intake") + ".");
            prescriptions.add("Prescription " + count + ": " + resultSet.getString("name") + " " + resultSet.getString("amount_in_grams") + " " + resultSet.getString("intake") + ".");
        }
        System.out.println();

        String prescriptionListJson = new Gson().toJson(prescriptions);
        return String.valueOf(prescriptionListJson);
    }
}
