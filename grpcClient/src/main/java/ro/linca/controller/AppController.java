package ro.linca.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.util.Duration;
import ro.linca.grpc.MedicationPlan;
import ro.linca.grpc.MedicationPlanServiceGrpc;

import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AppController implements Initializable
{
    private final String FETCH_TIME = "19:15:0";
    private final String TAKE_MEDICATION_DEADLINE = "19:15:20";

    @FXML
    Button medicationTakenButton;
    @FXML
    ListView<String> prescriptionList;
    @FXML
    Label timeLabel;
    private ObservableList<String> observablePrescriptionList = FXCollections.observableArrayList();

    public void initialize(URL location, ResourceBundle resources)
    {
        setUpClock();
    }

    private void fetchPrescriptions()
    {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 9090)
                .usePlaintext()
                .build();
        MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub planStub = MedicationPlanServiceGrpc.newBlockingStub(channel);
        MedicationPlan.GetPlanResponse response = planStub.getPlan(MedicationPlan.EmptyRequest.newBuilder().build());

        List<String> fetchedPrescriptions = new Gson().fromJson(response.getMedicationPlanJson(), new TypeToken<ArrayList<String>>(){}.getType());
        observablePrescriptionList.addAll(fetchedPrescriptions);
        prescriptionList.setItems(observablePrescriptionList);
    }

    private void setUpClock()
    {
        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e ->
        {
            LocalTime currentTime = LocalTime.now();
            String currentTimeString = currentTime.getHour() + ":" + currentTime.getMinute() + ":" + currentTime.getSecond();
            timeLabel.setText(currentTimeString);

            if(currentTimeString.equals(FETCH_TIME))
            {
                fetchPrescriptions();
            }

            if(currentTimeString.equals(TAKE_MEDICATION_DEADLINE) && !prescriptionList.getItems().isEmpty())
            {
                alertServer();
            }
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    public void onTakeMedication(ActionEvent actionEvent)
    {
        try
        {
            String takenMedication = prescriptionList.getSelectionModel().getSelectedItem();

            ManagedChannel channel = ManagedChannelBuilder
                    .forAddress("localhost", 9090)
                    .usePlaintext()
                    .build();
            MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub planStub = MedicationPlanServiceGrpc.newBlockingStub(channel);
            MedicationPlan.MedicationTakenResponse response = planStub.notifyServer(MedicationPlan.MedicationTakenRequest.newBuilder().setRequestMessage(takenMedication).build());
            System.out.println(response.getResponseMessage());

            observablePrescriptionList.remove(takenMedication);
        }
        catch(Exception e)
        {
            System.out.println("You must select a medication!");
        }
    }

    private void alertServer()
    {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 9090)
                .usePlaintext()
                .build();
        MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub planStub = MedicationPlanServiceGrpc.newBlockingStub(channel);
        MedicationPlan.MedicationNotTakenResponse response = planStub.alertServer(MedicationPlan.EmptyRequest.newBuilder().build());
        System.out.println(response.getResponseMessage());
    }
}
